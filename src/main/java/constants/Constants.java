package constants;

public class Constants {
    public static final int BOX_SIZE = 30;
    public static final int PREVIEW_BOX_SIZE = 20;

    // To change tetris size, change the two values below
    public static final int COLUMN_BOX_COUNT = 10;
    public static final int ROW_BOX_COUNT = 20;

    public static final int ROW_BOX_NUMBER = COLUMN_BOX_COUNT;

    public static final int MOVE_BLOCK_RATE = 1000;
    public static final int DEFAULT_BLOCK_CENTER = COLUMN_BOX_COUNT / 2;

    public static final int PREVIEW_BOX_ROW_COUNT = 6;
    public static final int PREVIEW_BOX_COLUMN_COUNT = 6;
    public static final int BLOCK_PREVIEW_CENTER = 5000 + PREVIEW_BOX_ROW_COUNT / 2 + 3 * ROW_BOX_NUMBER;
    public static final int BOARD_WIDTH = (COLUMN_BOX_COUNT + PREVIEW_BOX_COLUMN_COUNT) * 30;
    public static final int BOARD_HEIGHT = ROW_BOX_COUNT * BOX_SIZE + 50;
}
