package blocks;

import gameBoard.Box;

import java.util.ArrayList;
import java.util.Random;

public class BlockFactory {
    private static ArrayList<Block> blockList;
    private static Block nextBlock;
    private static Random random = new Random(System.nanoTime());

    private static void initializeBlocks(){
        blockList = new ArrayList<>();
        blockList.add(new Wood());
        blockList.add(new Window());
        blockList.add(new LeftLeg());
        blockList.add(new LeftDuck());
        blockList.add(new RightLeg());
        blockList.add(new RightDuck());
        blockList.add(new Mountain());
    }

    public static Block getNextBlock() {
        initializeBlocks();
        nextBlock = blockList.get(random.nextInt(7));
        return nextBlock;
    }


    public static Block getRandomBlock(){
        if(blockList == null){
            initializeBlocks();
            nextBlock = blockList.get(random.nextInt(7));
        }
        return nextBlock;
    }
}
