package blocks;

public enum Direction {
    LEFT,
    UP,
    RIGHT,
    DOWN;

    public Direction next(){
        return values()[(this.ordinal()+1)%values().length];
    }
}
