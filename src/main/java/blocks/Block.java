package blocks;

import constants.Constants;

import java.awt.*;
import java.util.Arrays;
import java.util.List;

public abstract class Block {
    Direction direction;
    int boxNumber;
    List<Integer> leftBoxNumbers;
    List<Integer> upBoxNumbers;
    List<Integer> rightBoxNumbers;
    List<Integer> downBoxNumbers;

    public Block() {
        direction = Direction.LEFT;
    }

    public void rotate() {
        direction = direction.next();
    }

    public List<Integer> getBoxNumbers(int boxNumber) {
        this.boxNumber = boxNumber;
        setBoxNumbers();
        switch (direction) {
            case LEFT:
                return leftBoxNumbers;
            case UP:
                return upBoxNumbers;
            case RIGHT:
                return rightBoxNumbers;
            case DOWN:
                return downBoxNumbers;
            default:
                return null;
        }
    }

    protected abstract void setBoxNumbers();


    public abstract Color getColor();
}

class Wood extends Block {

    public Wood() {
        super();
    }

    public void setBoxNumbers() {
        leftBoxNumbers = Arrays.asList(boxNumber - 1, boxNumber + Constants.ROW_BOX_NUMBER - 1,
                boxNumber - Constants.ROW_BOX_NUMBER - 1, boxNumber - 2 * Constants.ROW_BOX_NUMBER - 1);
        upBoxNumbers = Arrays.asList(boxNumber - Constants.ROW_BOX_NUMBER, boxNumber - Constants.ROW_BOX_NUMBER - 1,
                boxNumber - Constants.ROW_BOX_NUMBER - 2, boxNumber - Constants.ROW_BOX_NUMBER + 1);
        rightBoxNumbers = Arrays.asList(boxNumber, boxNumber + Constants.ROW_BOX_NUMBER,
                boxNumber - Constants.ROW_BOX_NUMBER, boxNumber - 2 * Constants.ROW_BOX_NUMBER);
        downBoxNumbers = Arrays.asList(boxNumber, boxNumber - 1,
                boxNumber - 2, boxNumber + 1);
    }

    @Override
    public Color getColor() {
        return new Color(190,190,0);
    }

}

class LeftLeg extends Block {
    public LeftLeg() {
        super();
        setBoxNumbers();
    }

    public void setBoxNumbers() {

        leftBoxNumbers = Arrays.asList(boxNumber, boxNumber + Constants.ROW_BOX_NUMBER - 1,
                boxNumber + Constants.ROW_BOX_NUMBER, boxNumber - Constants.ROW_BOX_NUMBER);
        upBoxNumbers = Arrays.asList(boxNumber, boxNumber - 1,
                boxNumber - Constants.ROW_BOX_NUMBER - 1, boxNumber + 1);
        rightBoxNumbers = Arrays.asList(boxNumber, boxNumber - Constants.ROW_BOX_NUMBER + 1,
                boxNumber + Constants.ROW_BOX_NUMBER, boxNumber - Constants.ROW_BOX_NUMBER);
        downBoxNumbers = Arrays.asList(boxNumber, boxNumber - 1,
                boxNumber + Constants.ROW_BOX_NUMBER + 1, boxNumber + 1);
    }

    @Override
    public Color getColor() {
        return Color.MAGENTA;
    }
}


class RightLeg extends Block {
    public RightLeg() {
        super();
    }

    public void setBoxNumbers() {

        leftBoxNumbers = Arrays.asList(boxNumber, boxNumber + Constants.ROW_BOX_NUMBER + 1,
                boxNumber + Constants.ROW_BOX_NUMBER, boxNumber - Constants.ROW_BOX_NUMBER);
        upBoxNumbers = Arrays.asList(boxNumber, boxNumber - 1,
                boxNumber + Constants.ROW_BOX_NUMBER - 1, boxNumber + 1);
        rightBoxNumbers = Arrays.asList(boxNumber, boxNumber - Constants.ROW_BOX_NUMBER - 1,
                boxNumber + Constants.ROW_BOX_NUMBER, boxNumber - Constants.ROW_BOX_NUMBER);
        downBoxNumbers = Arrays.asList(boxNumber, boxNumber - 1,
                boxNumber - Constants.ROW_BOX_NUMBER + 1, boxNumber + 1);
    }

    @Override
    public Color getColor() {
        return Color.RED;
    }
}

class Window extends Block {
    public Window() {
        super();
        setBoxNumbers();
    }

    public void setBoxNumbers() {
        leftBoxNumbers = upBoxNumbers = rightBoxNumbers = downBoxNumbers =
                Arrays.asList(boxNumber, boxNumber - Constants.ROW_BOX_NUMBER,
                        boxNumber - 1, boxNumber - Constants.ROW_BOX_NUMBER - 1);
    }

    @Override
    public Color getColor() {
        return new Color(102,51,0);
    }
}


class Mountain extends Block {
    public Mountain() {
        super();
        setBoxNumbers();
    }

    public void setBoxNumbers() {

        leftBoxNumbers = Arrays.asList(boxNumber, boxNumber + 1,
                boxNumber + Constants.ROW_BOX_NUMBER, boxNumber - 1);
        upBoxNumbers = Arrays.asList(boxNumber, boxNumber - Constants.ROW_BOX_NUMBER,
                boxNumber - 1, boxNumber + Constants.ROW_BOX_NUMBER);
        rightBoxNumbers = Arrays.asList(boxNumber, boxNumber + 1,
                boxNumber - Constants.ROW_BOX_NUMBER, boxNumber - 1);
        downBoxNumbers = Arrays.asList(boxNumber, boxNumber - Constants.ROW_BOX_NUMBER,
                boxNumber + 1, boxNumber + Constants.ROW_BOX_NUMBER);
    }

    @Override
    public Color getColor() {
        return Color.CYAN;
    }
}

class RightDuck extends Block {
    public RightDuck() {
        super();
        setBoxNumbers();
    }

    public void setBoxNumbers() {

        leftBoxNumbers = Arrays.asList(boxNumber, boxNumber + Constants.ROW_BOX_NUMBER - 1,
                boxNumber + Constants.ROW_BOX_NUMBER, boxNumber + 1);
        upBoxNumbers = Arrays.asList(boxNumber, boxNumber + Constants.ROW_BOX_NUMBER,
                boxNumber - Constants.ROW_BOX_NUMBER - 1, boxNumber - 1);
        rightBoxNumbers = Arrays.asList(boxNumber, boxNumber - Constants.ROW_BOX_NUMBER,
                boxNumber - Constants.ROW_BOX_NUMBER + 1, boxNumber - 1);
        downBoxNumbers = Arrays.asList(boxNumber, boxNumber + 1,
                boxNumber + Constants.ROW_BOX_NUMBER + 1, boxNumber - Constants.ROW_BOX_NUMBER);
    }

    @Override
    public Color getColor() {
        return new Color(0,102,102);
    }
}

class LeftDuck extends Block {
    public LeftDuck() {
        super();
        setBoxNumbers();
    }

    public void setBoxNumbers() {

        leftBoxNumbers = Arrays.asList(boxNumber, boxNumber + Constants.ROW_BOX_NUMBER + 1,
                boxNumber + Constants.ROW_BOX_NUMBER, boxNumber - 1);
        upBoxNumbers = Arrays.asList(boxNumber, boxNumber - Constants.ROW_BOX_NUMBER,
                boxNumber + Constants.ROW_BOX_NUMBER - 1, boxNumber - 1);
        rightBoxNumbers = Arrays.asList(boxNumber, boxNumber - Constants.ROW_BOX_NUMBER,
                boxNumber - Constants.ROW_BOX_NUMBER - 1, boxNumber + 1);
        downBoxNumbers = Arrays.asList(boxNumber, boxNumber + Constants.ROW_BOX_NUMBER,
                boxNumber + 1, boxNumber - Constants.ROW_BOX_NUMBER + 1);
    }

    @Override
    public Color getColor() {
        return Color.ORANGE;
    }
}



