package gameBoard;

import javax.swing.*;

public class Score {
    private static Score score;
    int points;
    int linesCleared;
    private JLabel label;
    private JLabel linesClearedLabel;

    private Score() {
        points = 0;
    }

    public static Score getInstance() {
        if (score == null) {
            score = new Score();
        }
        return score;
    }

    public void addPoints(int amount) {
        points += amount;
        changeScoreLabelValue();
    }

    public void addLines(int amount){
        linesCleared+=amount;
        changeLinesLabelValue();
    }

    public void clear() {
        points = 0;
        linesCleared = 0;
        changeScoreLabelValue();
        changeLinesLabelValue();
    }

    public void addScoreChangeListener(JLabel label) {
        this.label = label;
    }

    public void addLinesClearedListener(JLabel linesClearedLabel) {
        this.linesClearedLabel = linesClearedLabel;
    }

    private void changeLinesLabelValue() {
        linesClearedLabel.setText(String.valueOf(linesCleared));
    }

    private void changeScoreLabelValue() {
        label.setText(String.valueOf(points));
    }

    public int getPoints() {
        return points;
    }
}
