package gameBoard;

import blocks.Block;
import constants.Constants;
import main.GameState;

import java.awt.*;

public class Drawer {
    private Graphics2D graphics2D;

    public Drawer(Graphics2D graphics2D) {
        this.graphics2D = graphics2D;
        drawBoard();
    }

    private void fillRect(Box box, int boxSize) {
        graphics2D.fillRect(box.getCornerX(), box.getCornerY(), boxSize, boxSize);
        graphics2D.setColor(Color.BLACK);
        graphics2D.drawRect(box.getCornerX(), box.getCornerY(), boxSize, boxSize);
    }

    public void drawBoard() {
        for (Box box : GameState.getInstance().getBoxes().values()) {
            graphics2D.setColor(Color.WHITE);
            fillRect(box, Constants.BOX_SIZE);
        }

        for (Box box : GameState.getInstance().getPreviewBoxes().values()) {
            graphics2D.setColor(Color.WHITE);
            fillRect(box, Constants.PREVIEW_BOX_SIZE);
        }

        Block block = GameState.getInstance().getNextBlock();
        for (int boxNumber : block.getBoxNumbers(Constants.BLOCK_PREVIEW_CENTER)) {
            graphics2D.setColor(Color.YELLOW);
            fillRect(GameState.getInstance().getPreviewBoxes().get(boxNumber), Constants.PREVIEW_BOX_SIZE);
        }
    }

    public void moveBlock(int currentBlockCenter) {
        Block block = GameState.getInstance().getBlock();
        for (int boxNumber : block.getBoxNumbers(currentBlockCenter)) {
            if (boxNumber < 0 || boxNumber > Constants.ROW_BOX_COUNT * Constants.COLUMN_BOX_COUNT) {
                continue;
            }
            graphics2D.setColor(block.getColor());
            fillRect(GameState.getInstance().getBoxes().get(boxNumber), Constants.BOX_SIZE);
        }

        for (Integer boxNumber : GameState.getInstance().getSavedBlocks()) {
            graphics2D.setColor(Color.BLUE);
            fillRect(GameState.getInstance().getBoxes().get(boxNumber), Constants.BOX_SIZE);
        }
    }

    public void drawGameOver(Graphics2D graphics2D) {
        graphics2D.setColor(Color.WHITE);
        graphics2D.fillRect(0, 0, Constants.BOARD_WIDTH, Constants.BOARD_HEIGHT);
        graphics2D.setColor(Color.BLACK);

        String prompt = "Game Over! :(";
        Font font = new Font("Helvetica", Font.BOLD, 30);
        FontMetrics fontMetrics = graphics2D.getFontMetrics(font);
        int width = fontMetrics.stringWidth(prompt);
        graphics2D.setFont(font);
        graphics2D.drawString(prompt, (Constants.BOARD_WIDTH - width) / 2, (Constants.BOARD_HEIGHT - 50) / 2);

    }

    public void cleanBoard() {
        graphics2D.clearRect(0, 0, Constants.BOARD_HEIGHT, Constants.BOARD_WIDTH);
    }
}
