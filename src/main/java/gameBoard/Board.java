package gameBoard;

import blocks.BlockFactory;
import constants.Constants;
import main.GameState;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

public class Board extends JComponent {
    Timer timer;
    Drawer drawer;

    public Board() {
        setBoxes();
        GameState.getInstance().setRestartListener(this);
        GameState.getInstance().setCurrentBlockCenter(Constants.DEFAULT_BLOCK_CENTER);
        GameState.getInstance().setBlock(BlockFactory.getRandomBlock());
        GameState.getInstance().setNextBlock(BlockFactory.getNextBlock());
        addKeyListener(new MyKeyListener());

        timer = new Timer();
        TimerTask moveBlock = new MoveBlock();
        timer.scheduleAtFixedRate(moveBlock, 2000, Constants.MOVE_BLOCK_RATE);
    }

    private void setBoxes() {
        int counter = 0;
        HashMap<Integer, Box> boxes = new HashMap<>();
        for (int i = 0; i < Constants.BOX_SIZE * Constants.ROW_BOX_COUNT; i += Constants.BOX_SIZE) {
            for (int j = 0; j < Constants.BOX_SIZE * Constants.COLUMN_BOX_COUNT; j += Constants.BOX_SIZE) {
                boxes.put(counter, new Box(j, i));
                counter++;
            }
        }
        GameState.getInstance().setBoxes(boxes);

        HashMap<Integer, Box> previewBoxes = new HashMap<>();
        counter = 5000;
        int counter2 = 0;
        for (int i = Constants.BOARD_HEIGHT / 2;
             i < Constants.PREVIEW_BOX_ROW_COUNT * Constants.PREVIEW_BOX_SIZE + Constants.BOARD_HEIGHT / 2;
             i += Constants.PREVIEW_BOX_SIZE) {
            for (int j = Constants.BOX_SIZE * Constants.COLUMN_BOX_COUNT;
                 j < Constants.PREVIEW_BOX_COLUMN_COUNT * Constants.PREVIEW_BOX_SIZE + Constants.BOX_SIZE * Constants.COLUMN_BOX_COUNT;
                 j += Constants.PREVIEW_BOX_SIZE) {
                previewBoxes.put(counter + counter2, new Box(j + 20, i+50));
                counter2++;
            }
            counter2 = 0;
            counter += Constants.ROW_BOX_NUMBER;
        }
        GameState.getInstance().setPreviewBoxes(previewBoxes);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D graphics2D = (Graphics2D) g;
        requestFocus();
        drawer = new Drawer(graphics2D);
        if (GameState.getInstance().isGameOver()) {
            drawer.drawGameOver(graphics2D);
            timer.cancel();
            return;
        }
        drawer.moveBlock(GameState.getInstance().getCurrentBlockCenter());
    }

    private class MoveBlock extends TimerTask {
        @Override
        public void run() {
            checkBlockReached();
            int moveAmount = Constants.ROW_BOX_NUMBER;
//            if (GameState.getInstance().DoesCollide(moveAmount)) return;
            move(moveAmount);
            checkClearARow();
            repaint();
        }
    }

    private void checkIfLost() {
        GameState.getInstance().checkIfLost();
    }

    private void checkClearARow() {
        GameState.getInstance().checkClearARow();
    }

    private void checkBlockReached() {
        for (Integer blockNumber : GameState.getInstance().getBlockNumbers()) {
            if (blockNumber + Constants.ROW_BOX_NUMBER >= GameState.getInstance().getBoxes().size() ||
                    GameState.getInstance().getSavedBlocks().contains(blockNumber + Constants.ROW_BOX_NUMBER)) {
                checkIfLost();
                GameState.getInstance().saveCurrentBlock();
                GameState.getInstance().setBlock(BlockFactory.getRandomBlock());
                GameState.getInstance().setNextBlock(BlockFactory.getNextBlock());
                GameState.getInstance().setCurrentBlockCenter(Constants.DEFAULT_BLOCK_CENTER);
                break;
            }
        }
    }

    class MyKeyListener extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent e) {
            int moveAmount;
            switch (e.getKeyCode()) {
                case KeyEvent.VK_RIGHT:
                    move(1);
                    repaint();
                    break;
                case KeyEvent.VK_LEFT:
                    move(-1);
                    repaint();
                    break;
                case KeyEvent.VK_UP:
                    GameState.getInstance().rotateCurrentBlock();
                    repaint();
                    break;
                case KeyEvent.VK_DOWN:
                    moveAmount = Constants.ROW_BOX_NUMBER;
                    move(moveAmount);
                    checkBlockReached();
                    repaint();
                    checkClearARow();
                    break;
            }
        }

    }
    private void move(int moveAmount) {
        if (!GameState.getInstance().DoesCollide(moveAmount))
            GameState.getInstance().moveCurrentBlockCenter(moveAmount);
    }
}


