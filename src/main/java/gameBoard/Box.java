package gameBoard;

import constants.Constants;

public class Box {
    private int cornerX;
    private int cornerY;

    public Box(int cornerX, int cornerY) {
        this.cornerX = cornerX;
        this.cornerY = cornerY;
    }

    public int getCornerX() {
        return cornerX;
    }

    public int getCornerY() {
        return cornerY;
    }

}
