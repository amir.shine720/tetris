package main;

import blocks.Block;
import blocks.BlockFactory;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import constants.Constants;
import gameBoard.Board;
import gameBoard.Box;
import gameBoard.Score;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.*;

public class GameState {
    private static GameState gameState;

    private int currentBlockCenter;
    private boolean gameOver;
    private Board boardListener;
    private Block block;
    private Block nextBlock;
    private Map<Integer, Box> boxes;
    private Map<Integer, Box> previewBoxes;
    private List<Integer> savedBlocks = new ArrayList<>();
    private List<JButton> buttonListeners;

    public boolean isGameOver() {
        return gameOver;
    }

    public void setGameOver(boolean gameOver) {
        this.gameOver = gameOver;
    }

    public static GameState getInstance() {
        if (gameState == null) gameState = new GameState();
        return gameState;
    }

    public Map<Integer, Box> getBoxes() {
        return boxes;
    }

    public void setBoxes(Map<Integer, Box> boxes) {
        this.boxes = boxes;
    }

    public Block getBlock() {
        return block;
    }

    public void setBlock(Block block) {
        this.block = block;
    }

    public int getCurrentBlockCenter() {
        return currentBlockCenter;
    }

    public void setCurrentBlockCenter(int currentBlockCenter) {
        this.currentBlockCenter = currentBlockCenter;
    }

    public void moveCurrentBlockCenter(int moveAmount) {
        currentBlockCenter += moveAmount;
    }

    public boolean DoesCollide(int moveAmount) {
        for (Integer blockNumber : getBlockNumbers()) {
            if ((moveAmount == 1 || moveAmount == -1) && getBoxRow(blockNumber) != getBoxRow(blockNumber + moveAmount))
                return true;
            if (savedBlocks.contains(moveAmount + blockNumber)) return true;
            if (blockNumber + moveAmount > getBoxes().size()) return true;
        }
        return false;
    }

    public void rotateCurrentBlock() {
        List<Integer> previousBlockNumbers = getBlockNumbers();
        block.rotate();
        checkForOutOfBoundRotation(previousBlockNumbers);
    }

    private void checkForOutOfBoundRotation(List<Integer> previousBlockNumbers) {
        for (Integer previousBlockNumber : previousBlockNumbers) {
            for (Integer blockNumber : getBlockNumbers()) {
                if (getBoxRow(blockNumber) < 0) {
                    moveCurrentBlockCenter(Constants.ROW_BOX_NUMBER);
                    checkForOutOfBoundRotation(previousBlockNumbers);
                    return;
                }
                if (getBoxColumn(previousBlockNumber) - getBoxColumn(blockNumber) <= -8) {
                    moveCurrentBlockCenter(1);
                    checkForOutOfBoundRotation(previousBlockNumbers);
                    return;
                }
                if (getBoxColumn(previousBlockNumber) - getBoxColumn(blockNumber) >= 8) {
                    moveCurrentBlockCenter(-1);
                    checkForOutOfBoundRotation(previousBlockNumbers);
                    return;
                }
                if (getSavedBlocks().contains(blockNumber)) {
                    for (int i = 0; i < 3; i++) {
                        block.rotate();
                    }
                }
            }
        }
    }

    public List<Integer> getSavedBlocks() {
        return savedBlocks;
    }

    public void saveCurrentBlock() {
        if (savedBlocks == null) {
            savedBlocks = new ArrayList<>();
        }
        Score.getInstance().addPoints(1);
        savedBlocks.addAll(gameState.block.getBoxNumbers(currentBlockCenter));
    }

    public List<Integer> getBlockNumbers() {
        return block.getBoxNumbers(currentBlockCenter);
    }

    private int getBoxColumn(int boxNumber) {
        // For when we want to rotate in the very beginning
        if (boxNumber < 0) {
            return Constants.ROW_BOX_NUMBER / 2;
        }
        return boxNumber % Constants.ROW_BOX_NUMBER;
    }

    private int getBoxRow(int boxNumber) {
        // For when we want to rotate in the very beginning
        if (boxNumber < 0) {
            return -Constants.ROW_BOX_NUMBER;
        }
        return boxNumber / Constants.ROW_BOX_NUMBER;
    }

    public void checkClearARow() {
        if (savedBlocks.isEmpty()) return;
        for (int i = getBoxes().size() - 1; i >= Collections.min(savedBlocks); i -= Constants.ROW_BOX_NUMBER) {
            int counter = 0;
            for (int j = 0; j < Constants.ROW_BOX_NUMBER; j++) {
                if (!savedBlocks.contains(i - j)) {
                    break;
                }
                counter++;
            }
            if (counter == Constants.ROW_BOX_NUMBER) {
                deleteSavedRow(i);
                Score.getInstance().addPoints(Constants.ROW_BOX_NUMBER);
                Score.getInstance().addLines(1);
                checkClearARow();
                return;
            }
        }
    }

    private void deleteSavedRow(int row) {
        for (int i = 0; i < Constants.ROW_BOX_NUMBER; i++) {
            savedBlocks.remove(Integer.valueOf(row - i));
        }
        for (Integer number : savedBlocks) {
            if (number < row) {
                savedBlocks.set(savedBlocks.indexOf(number), number + Constants.ROW_BOX_NUMBER);
            }
        }
    }

    public void checkIfLost() {
        if (gameOver) {
            return;
        }
        for (Integer savedBlock : getBlockNumbers()) {
            if (savedBlock < 0) {
                setGameOver(true);
                savePlayerScore();
                hideButtons();
                return;
            }
        }
    }

    private void hideButtons() {
        for (JButton buttonListener : buttonListeners) {
            buttonListener.setVisible(false);
        }
    }

    public ArrayList<Integer> getScores() {
        Gson g = new Gson();
        Type type = new TypeToken<ArrayList<Integer>>() {
        }.getType();
        ArrayList<Integer> scores;
        try (BufferedReader br = new BufferedReader(new FileReader("./src/main/scores.json"))) {
            scores = (g.fromJson(br, type));
        } catch (NullPointerException | IOException e) {
            scores = new ArrayList<>();
        }
        return scores;
    }

    private void savePlayerScore() {
        Gson gson = new Gson();
        ArrayList<Integer> scores = getScores();
        scores.add(Score.getInstance().getPoints());
        String json = gson.toJson(scores);
        String path = "src/main/scores.json";
        try (FileWriter writer = new FileWriter(path)) {
            writer.write(json);
        } catch (IOException ignored) {
        }
    }

    public void reset() {
        block = BlockFactory.getNextBlock();
        nextBlock = BlockFactory.getNextBlock();
        currentBlockCenter = Constants.DEFAULT_BLOCK_CENTER;
        savedBlocks.clear();
        Score.getInstance().clear();
        boardListener.repaint();
    }

    public void setRestartListener(Board board) {
        boardListener = board;
    }

    public void repent() {
        setCurrentBlockCenter(Constants.DEFAULT_BLOCK_CENTER);
        boardListener.repaint();
    }

    public Map<Integer, Box> getPreviewBoxes() {
        return previewBoxes;
    }

    public Block getNextBlock() {
        return nextBlock;
    }

    public void setPreviewBoxes(HashMap<Integer, Box> previewBoxes) {
        this.previewBoxes = previewBoxes;
    }

    public void setNextBlock(Block nextBlock) {
        this.nextBlock = nextBlock;
    }


    public void setButtonGameOverListener(JButton button) {
        if(buttonListeners == null) {
            buttonListeners = new ArrayList<>();
        }
        buttonListeners.add(button);
    }
}
