package main;

import constants.Constants;
import gameBoard.Board;
import gameBoard.Score;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Collections;

public class GUICreator {
    JFrame frame;
    JPanel scorePanel;
    JPanel linesClearedPanel;
    JButton resetGameButton;
    JButton repentanceButton;
    JPanel rootPanel;
    JList<Object> scoreJList;
    JPanel gameDataPanel;

    public void start() {
        initializeGame();
    }

    private void initializeGame() {
        frame = new JFrame();
        setFrame();
        setScorePanel();
        setLinesClearedPanel();
        setRestartButton();
        setRepentanceButton();
        setRootPanel();
        setScoreList();
        setGameDataPanel();

        rootPanel.add(gameDataPanel, BorderLayout.EAST);
        frame.add(rootPanel);
    }

    private void setFrame() {
        Board board = new Board();
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.getContentPane().add(board);
        frame.requestFocus();
        frame.setSize(Constants.BOARD_WIDTH, Constants.BOARD_HEIGHT);
        frame.setTitle("Tetris");
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setVisible(true);
    }

    private void setScorePanel() {
        scorePanel = new JPanel();
        scorePanel.setLayout(new FlowLayout());
        JLabel scoreLabel = new JLabel(String.valueOf(0));
        scorePanel.add(new JLabel("Score: "));
        scorePanel.add(scoreLabel);
        Score.getInstance().addScoreChangeListener(scoreLabel);
    }

    private void setLinesClearedPanel() {
        linesClearedPanel = new JPanel();
        linesClearedPanel.setLayout(new FlowLayout());
        JLabel linesClearedLabel = new JLabel(String.valueOf(0));
        linesClearedPanel.add(new JLabel("Lines cleared: "));
        linesClearedPanel.add(linesClearedLabel);
        linesClearedPanel.setBorder(new EmptyBorder(0,45,0,0));
        Score.getInstance().addLinesClearedListener(linesClearedLabel);
    }

    private void setRestartButton() {
        resetGameButton = new JButton();
        resetGameButton.setMaximumSize(new Dimension(150, 50));
        resetGameButton.setText("Restart");
        resetGameButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if (GameState.getInstance().isGameOver()) {
                    new Board();
                }
                GameState.getInstance().reset();
            }
        });
        GameState.getInstance().setButtonGameOverListener(resetGameButton);
    }

    private void setRepentanceButton() {
        repentanceButton = new JButton();
        repentanceButton.setMaximumSize(new Dimension(150, 50));
        repentanceButton.setText("Reset block");
        repentanceButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                GameState.getInstance().repent();
            }
        });
        GameState.getInstance().setButtonGameOverListener(repentanceButton);
    }

    private void setRootPanel() {
        rootPanel = new JPanel();
        rootPanel.setLayout(new BorderLayout());
        rootPanel.setSize(frame.getSize());
    }

    private void setScoreList() {
        ArrayList<Integer> scoreList = GameState.getInstance().getScores();
        ArrayList<String> scoreString = getScoreStrings(scoreList);
        scoreJList = new JList<>(scoreString.toArray());
        scoreJList.setBorder(new EmptyBorder(0,40,0,0));
        scoreJList.setOpaque(false);
    }

    private ArrayList<String> getScoreStrings(ArrayList<Integer> scoreList) {
        Collections.sort(scoreList);
        Collections.reverse(scoreList);
        ArrayList<String> scoreString = new ArrayList<>();
        for (int i = 1; i <= scoreList.size(); i++) {
            if (i == 11) {
                break;
            }
            scoreString.add(i + "- " + scoreList.get(i-1));
        }
        return scoreString;
    }

    private void setGameDataPanel() {
        gameDataPanel = new JPanel();
        gameDataPanel.setBorder(BorderFactory.createEmptyBorder(25, 0, 0, 15));

        Box box = Box.createVerticalBox();
        box.add(scorePanel);
        box.add(Box.createRigidArea(new Dimension(0, 5)));
        box.add(linesClearedPanel);
        box.add(Box.createRigidArea(new Dimension(0, 5)));
        box.add(resetGameButton);
        box.add(Box.createRigidArea(new Dimension(0, 5)));
        box.add(repentanceButton);
        box.add(Box.createRigidArea(new Dimension(0, 5)));
        box.add(new JLabel("The top scores are: "));
        box.add(Box.createRigidArea(new Dimension(0, 5)));

        box.add(scoreJList);

        gameDataPanel.add(box);
    }
}
